var assert = require('assert');
var lib = require('../lib/h.lib')
describe('padLeft', function() {
  it('passing a 3 digit number with total desired length 6', function() {
    var ret = lib.padLeft(235, 6);
    assert.equal(ret, "000235");
  });

  it('passing a 6 digit number with total desired length 3', function() {
    var ret = lib.padLeft(164235, 3);
    assert.equal(ret, "164235");
  });

  it('passing a 3 digit number with total desired length negative', function() {
    var ret = lib.padLeft(164235, -3);
    assert.equal(ret, "Err with input(s)");
  });

  it('passing a negative 3 digit number with total desired length 5', function() {
    var ret = lib.padLeft(-235, 5);
    assert.equal(ret, "-00235");
  });

  it('passing a NaN with total desired length 5', function() {
    var ret = lib.padLeft("gadbad", 5);
    assert.equal(ret, "Err with input(s)");
  });

  it('passing a 3 digit number with total desired length NaN', function() {
    var ret = lib.padLeft(235, "ghotala");
    assert.equal(ret, "Err with input(s)");
  });

  it('passing a floating point number with total desired length 0', function() {
    var ret = lib.padLeft(235.54, 3);
    assert.equal(ret, "only integers supported");
  });

  
  
});
