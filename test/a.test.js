///   If the integer is divisible by 3, return the string "Java".
//   If the integer is divisible by 3 and divisible by 4, return the string"Coffee"
//   If the integer is one of the above and is even, add "Script" to the end of the string.
//   Otherwise, return the string "mocha_missing!"

var assert = require('assert');
var lib = require('../lib/a.lib')
describe('caffeineBuzz', function() {
  it('Passing 0 as input', function() {
    var ret = lib.caffeineBuzz(0);
    assert.equal(ret, "CoffeeScript");
  });
  it('Passing 1 as input', function() {
    var ret = lib.caffeineBuzz(1);
    assert.equal(ret, "mocha_missing");
  });
  it('Passing 2 as input', function() {
    var ret = lib.caffeineBuzz(2);
    assert.equal(ret, "mocha_missing");
  });
  it('Passing 3 as input', function() {
    var ret = lib.caffeineBuzz(3);
    assert.equal(ret, "Java");
  });
  it('Passing 4 as input', function() {
    var ret = lib.caffeineBuzz(4);
    assert.equal(ret, "mocha_missing");
  });
  it('Passing 12 as input', function() {
    var ret = lib.caffeineBuzz(12);
    assert.equal(ret, "CoffeeScript");
  });
  it('Passing negative number as input', function() {
    var ret = lib.caffeineBuzz(-12);
    assert.equal(ret, "CoffeeScript");
  });
});
