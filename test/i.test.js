var assert = require('assert');

var lib = require('../lib/i.lib')

describe('generatePassword', function() {
  it('passing empty spec object', function() {
    //it should consider pre defined minium char count for each prop as per 
    var specObj = {};
    var ret = lib.generatePassword(specObj);
  });

  it('passing spec object with all required properties correctly defined with equal values', function() {
    var specObj = {
      lowerCaseCharCount: 4,
      upperCaseCharCount: 4,
      specialCharCount: 4,
      numericalCharCount: 4,
      desiredCharCount: 16
    };

    var ret = lib.generatePassword(specObj);

  });

  it('passing spec object with all required properties correctly defined with different values', function() {
    var specObj = {
      lowerCaseCharCount: 2,
      upperCaseCharCount: 4,
      specialCharCount: 2,
      numericalCharCount: 4,
      desiredCharCount: 12
    };

    var ret = lib.generatePassword(specObj);

  });

  it('passing spec object with prop values lesser than min expected values', function() {
    var specObj = {
      lowerCaseCharCount: 1,
      upperCaseCharCount: 1,
      specialCharCount: 1,
      numericalCharCount: 1,
      desiredCharCount: 4
    };

    var ret = lib.generatePassword(specObj);
    assert.equal(ret.length, 8);
  });
  
  it('passing spec object with desired char count that does not match as count of other provided properties #1', function() {
    //in this case, all other properties should accomodate extra characters equally to match provided desired Char Count
    var specObj = {
      lowerCaseCharCount: 4,
      upperCaseCharCount: 4,
      specialCharCount: 4,
      numericalCharCount: 4,
      desiredCharCount: 20
    };

    var ret = lib.generatePassword(specObj);
  });

  it('passing spec object with desired char count that does not match as count of other provided properties #2', function() {
    //in this case, all other properties should accomodate extra characters equally to match provided desired Char Count
    var specObj = {
      lowerCaseCharCount: 2,
      upperCaseCharCount: 4,
      specialCharCount: 2,
      numericalCharCount: 4,
      desiredCharCount: 18
    };

    var ret = lib.generatePassword(specObj);
  });

  it('passing spec object with only lowercase property defined and desired char count provided', function() {
    //default values will be considered for unprovided properties (as per min-spec object) and count will be calculated after that 
    var specObj = {
      lowerCaseCharCount: 5,
      desiredCharCount: 18
    };

    var ret = lib.generatePassword(specObj);
  });

  it('passing spec object with only uppercase property defined and desired char count provided', function() {
    //default values will be considered for unprovided properties (as per min-spec object) and count will be calculated after that 
    var specObj = {
      upperCaseCharCount: 5,
      desiredCharCount: 18
    };

    var ret = lib.generatePassword(specObj);
  });

  it('passing spec object with only specialCharCount property defined and desired char count provided', function() {
    //default values will be considered for unprovided properties (as per min-spec object) and count will be calculated after that 
    var specObj = {
      specialCharCount: 5,
      desiredCharCount: 18
    };

    var ret = lib.generatePassword(specObj);
  });

  it('passing spec object with only numericalCharCount property defined and desired char count provided', function() {
    //default values will be considered for unprovided properties (as per min-spec object) and count will be calculated after that 
    var specObj = {
      numericalCharCount: 5,
      desiredCharCount: 18
    };

    var ret = lib.generatePassword(specObj);
  });

  it('verifying whether passwords are truly random, generating 50 passwords and comparing with each other', function() {
    //logic should not only contain string equality comparison but also indices comparison of char categories, e.g. lowercase chars should not be at same indices in all passwords and so on
    var specObj = {
      lowerCaseCharCount: 4,
      upperCaseCharCount: 4,
      specialCharCount: 4,
      numericalCharCount: 4,
      desiredCharCount: 16
    };

    var generatedPasswords = [];

    for(var i = 0; i < 50; i++){
      generatedPasswords.push(lib.generatePassword(specObj));
    }
        
  });


});
