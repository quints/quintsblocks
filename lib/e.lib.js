//5. Reverse a string in-place, and append an integer to each word  i.e., change "I am a good" to "doog1 a2 ma3 I4"
String.prototype.reverse = function(){
  var ret = [];
  for(var i = this.length - 1; i >= 0; i--){
    ret.push(this[i]);
  }
  return ret.join("");
};

String.prototype.appendOneBasedIndexToEachWord = function(){
  var ret = [];
  this.split(" ").forEach(function(currentWord, index, array){
    ret.push(currentWord + "" + (++index));
  });
  return ret.join(" ");
};

var reverseAndAppendOneBasedIndexToEachWord = function(inputStr){
  return inputStr.reverse().appendOneBasedIndexToEachWord();
}

//TODO - I did not understand in-place part. is it reversing string without creating another variable

// 6. Write a function to verify a word as a palindrome.
String.prototype.isPalindrome = function(){
  var reverse = this.reverse();
  return this.toUpperCase() == reverse.toUpperCase();
};

exports.reverseAndAppendOneBasedIndexToEachWord = reverseAndAppendOneBasedIndexToEachWord;
