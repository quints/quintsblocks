// 2. Identify and remove duplicates from an array/list.

//speaking of objects
var doesObjectResideInArray = function(unqArr, objectToSearch){
  var ret = false;
  for (var i in unqArr) {
    if(unqArr[i] instanceof Object && unqArr.hasOwnProperty(i)){
      ret = true;
      for(var prop in unqArr[i]){
        if(objectToSearch[prop] === undefined){
          ret = false;
          break;
        } else {
          if(!(unqArr[i][prop] === objectToSearch[prop])){
            ret = false;
            break;
          }
        }
      }
      if(ret == true){
        return ret;
      }
    }
  }
  return ret;
};

Array.prototype.unique = function(){

  var ret = [];

  for(var i = 0; i < this.length; i++){
    if(!(this[i] instanceof Function)){
      if(this[i] instanceof Object){
        if(!doesObjectResideInArray(ret, this[i])){
          ret.push(this[i]);
        }
      }
      else{
        if(ret.indexOf(this[i]) === -1){
          ret.push(this[i]);
        }
      }
    }
  }

  return ret;
};