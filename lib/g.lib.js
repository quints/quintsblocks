// 7. An unsorted array has 1 to 1000 numbers, except one number. Find the missing number.

Array.prototype.sum = function(){
  var sum = 0;
  for (var i = 0; i < this.length; i++) {
    sum += this[i];
  }
  return sum;
}

var sumOf1ToNNumbers = function(n){
  var sum = (n * (1 + n)) / 2;
  return sum;
}



var findMissingNumber = function(inputArr){
  return sumOf1ToNNumbers(10) - inputArr.sum();
};

exports.findMissingNumber = findMissingNumber;
