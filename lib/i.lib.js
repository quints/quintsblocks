// 9. Write a password generator, given input specifications like:
// Should have {some integer value} upper case alphabets
// Should have {some integer value} lower case alphabets
// Should have {some integer value} special characters
// Should have {some integer value} amount of numbers
// Should be {some integer value} in length
// The password should be fairly random each time the generate function is called.

//duh to myself
Array.prototype.swap = function(i, j){
  var temp = this[i];
  this[i] = this[j];
  this[j] = temp;
}

//something like Fisher–Yates shuffle https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
String.prototype.jumble = function(){
  var charArr = this.split("");
  var randomNumbersForSwap = [];
  for(var i = 0; i < charArr.length; i++){
    randomNumbersForSwap.push(getRandomInt(0, this.length));
    charArr.swap(i, randomNumbersForSwap[i]);
  }
  return charArr.join("");
};

//copied as is from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
var getRandomInt = function(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

var getNRandomChars = function(srcCharset, n){
  var ret = [];

  var randomNumbersArr = [];
  for (var i = 0; i < n; i++) {
    randomNumbersArr.push(getRandomInt(0, srcCharset.length));
    ret.push(srcCharset[randomNumbersArr[i]]);
  }
  return ret.join("");

};
var lowerCaseCharset = "abcdefghijklmnopqrstuvwxyz";
var upperCaseCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var specialCharset = "~!@#$%^&*()_+-={}|[]:;,.";
//var specialCharset = "~!@#$%^&*()_+`-={}|[]\\:\";'<>?,./";
var numericalCharset = "0123456789";

var minSpecObj = {
  lowerCaseCharCount: 2,
  upperCaseCharCount: 2,
  specialCharCount: 2,
  numericalCharCount: 2
};

var generatePassword = function(specObj){
  //finalize spec obj, in case of missing/incorrect spec
  var finalCharCount = 0;
  for(var prop in minSpecObj){
    if(!specObj.hasOwnProperty(prop) || specObj[prop] < minSpecObj[prop]){
      specObj[prop] = minSpecObj[prop];
    }    
    finalCharCount += specObj[prop];    
  }
  //if desired char count is bigger than final char count, we will take extra chars from each defined charset
  if(specObj.desiredCharCount && (specObj.desiredCharCount > finalCharCount)){
    var extraRequiredChars = specObj.desiredCharCount - finalCharCount;      

    while(extraRequiredChars > 0){
      for(var prop in minSpecObj){
        ++(specObj[prop]);
        --extraRequiredChars;
        if(extraRequiredChars == 0) {
          break;
        }
      }
    }
  }
  //DONE: finalized spec obj

  var lowerCaseChars = getNRandomChars(lowerCaseCharset, specObj.lowerCaseCharCount);
  var upperCaseChars = getNRandomChars(upperCaseCharset, specObj.upperCaseCharCount);
  var specialCaseChars = getNRandomChars(specialCharset, specObj.specialCharCount);
  var numericalChars = getNRandomChars(numericalCharset, specObj.numericalCharCount);

  var desiredPassword = (lowerCaseChars + upperCaseChars + specialCaseChars + numericalChars).jumble();
  
  return desiredPassword;
};

exports.generatePassword = generatePassword;
