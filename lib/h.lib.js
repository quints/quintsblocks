// 8. Write a function that takes an integer and left pads zeros to it to the limit specified as an argument.
// Example: func padLeft(235,5) -> 00235
//          func padLeft(235,3) -> 235
//          func padLeft(235,6) -> 000235


var padLeft = function(inputNum, desiredLength){
  if(isNaN(inputNum) || isNaN(desiredLength) || desiredLength < 0){
    return "Err with input(s)";
  }

  

  if((inputNum + "").indexOf(".") > -1){
    return "only integers supported";
  }


  var isNegative = false;
  if(inputNum < 0){
    isNegative = true;
    inputNum = inputNum * -1;
  }

  var ret = inputNum + "";
  

  if(desiredLength > ret.length){
    var paddingChar = "0";
    var requiredPaddingCount = desiredLength - ret.length;
    var paddingStr = (new Array(requiredPaddingCount + 1)).join(paddingChar);
    ret = paddingStr + ret;
  }

  return isNegative ? "-" + ret : ret;
};

exports.padLeft = padLeft;