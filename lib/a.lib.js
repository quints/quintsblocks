// 1.
// Complete the function caffeineBuzz, which takes a non-zero integer as it's one argument.
//   If the integer is divisible by 3, return the string "Java".
//   If the integer is divisible by 3 and divisible by 4, return the string"Coffee"
//   If the integer is one of the above and is even, add "Script" to the end of the string.
//   Otherwise, return the string "mocha_missing!"
//   caffeineBuzz(1)   => "mocha_missing!"
//   caffeineBuzz(3)   => "Java"
//   caffeineBuzz(6)   => "JavaScript"
//   caffeineBuzz(12)  => "CoffeeScript"

var isDivisibleBy = function(inputNum, divisibleBy){
  return inputNum % divisibleBy == 0;
};

var caffeineBuzz = function(inputNum){

  var inputNum = Number(inputNum);

  //validate input
  if(isNaN(inputNum) === true){
    return "This is Not a Number";
  }
  else {
    //main logic as per requirement
    var ret = "mocha_missing";

    if(isDivisibleBy(inputNum, 3) === true){
      ret = "Java";

      if(isDivisibleBy(inputNum, 4) === true){
        ret = "Coffee";
      }

      if(isDivisibleBy(inputNum, 2) === true){
        ret += "Script";
      }
    }

    return ret;
  }
};

exports.caffeineBuzz = caffeineBuzz;
