// 3. Given 2 arrays find the values not present in the second one. eg [1,2,3,4,5] [4,5,6,7,8] = 1,2,3
// 4. Given 2 arrays find the values common in both. eg [1,2,3,4,5] [4,5,6,7,8] = 5

var diffModesEnum = {
  uniqueInArray1 : 1,
  uniqueInArray2 : 2,
  intersection : 4
};

Array.prototype.compareForUniqueness = function(anotherArr){
  // 3. Given 2 arrays find the values not present in the second one. eg [1,2,3,4,5] [4,5,6,7,8] = 1,2,3
  var ret = [];
  for(var i = 0; i < this.length; i++){
    if(anotherArr.indexOf(this[i]) == -1){
      ret.push(this[i]);
    }
  }
  return ret;
};

Array.prototype.intersect = function(anotherArr){
  // 4. Given 2 arrays find the values common in both. eg [1,2,3,4,5] [4,5,6,7,8] = 5
  var ret = [];
  for(var i = 0; i < this.length; i++){
    if(anotherArr.indexOf(this[i]) > -1){
      ret.push(this[i]);
    }
  }
  return ret;
};

Array.prototype.diff = function(anotherArr, diffConfig){
  if(diffConfig == undefined){
    diffConfig = diffModesEnum.uniqueInArray1 | diffModesEnum.uniqueInArray2 | diffModesEnum.intersection;
  }

  var ret = {};

  if(diffConfig & diffModesEnum.uniqueInArray1){
    ret.uniqueInArray1 = this.compareForUniqueness(anotherArr);
  }
  if(diffConfig & diffModesEnum.uniqueInArray2){
    ret.uniqueInArray2 = anotherArr.compareForUniqueness(this);
  }
  if(diffConfig & diffModesEnum.intersection){
    ret.intersection = this.intersect(anotherArr);
  }

  return ret;

};


exports.diffModesEnum = diffModesEnum;