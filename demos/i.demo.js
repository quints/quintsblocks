// 9. Write a password generator, given input specifications like:
// Should have {some integer value} upper case alphabets
// Should have {some integer value} lower case alphabets
// Should have {some integer value} special characters
// Should have {some integer value} amount of numbers
// Should be {some integer value} in length
// The password should be fairly random each time the generate function is called.

var lib = require('../lib/i.lib');

var specObj1 = {
  lowerCaseCharCount: 4,
  upperCaseCharCount: 4,
  specialCharCount: 4,
  numericalCharCount: 4,
  desiredCharCount: 16
};
console.log(lib.generatePassword(specObj1));
console.log(lib.generatePassword(specObj1));
console.log(lib.generatePassword(specObj1));
console.log(lib.generatePassword(specObj1));

var specObj2 = {
  lowerCaseCharCount: 2,
  upperCaseCharCount: 4,
  specialCharCount: 2,
  numericalCharCount: 4,
  desiredCharCount: 12
};
console.log(lib.generatePassword(specObj2));
console.log(lib.generatePassword(specObj2));
console.log(lib.generatePassword(specObj2));
console.log(lib.generatePassword(specObj2));

var specObj3 = {
  lowerCaseCharCount: 2,
  upperCaseCharCount: 4,
  specialCharCount: 2,
  numericalCharCount: 4,
  desiredCharCount: 18
};
console.log(lib.generatePassword(specObj3));
console.log(lib.generatePassword(specObj3));
console.log(lib.generatePassword(specObj3));
console.log(lib.generatePassword(specObj3));

var specObj4 = {
  lowerCaseCharCount: 5,
  desiredCharCount: 18
};
console.log(lib.generatePassword(specObj4));
console.log(lib.generatePassword(specObj4));
console.log(lib.generatePassword(specObj4));
console.log(lib.generatePassword(specObj4));

var specObj5 = {
  lowerCaseCharCount: 7,
  desiredCharCount: 18
};
console.log(lib.generatePassword(specObj5));
console.log(lib.generatePassword(specObj5));
console.log(lib.generatePassword(specObj5));
console.log(lib.generatePassword(specObj5));

var specObj6 = {
  lowerCaseCharCount: 27
};
console.log(lib.generatePassword(specObj6));
console.log(lib.generatePassword(specObj6));
console.log(lib.generatePassword(specObj6));
console.log(lib.generatePassword(specObj6));

var specObj7 = {
  lowerCaseCharCount: 10,
  upperCaseCharCount: 10,
  numericalCharCount: 10,
  specialCharCount: 3,
  
};
console.log(lib.generatePassword(specObj7));
console.log(lib.generatePassword(specObj7));
console.log(lib.generatePassword(specObj7));
console.log(lib.generatePassword(specObj7));