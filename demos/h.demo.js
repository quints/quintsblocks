// 8. Write a function that takes an integer and left pads zeros to it to the limit specified as an argument.
// Example: func padLeft(235,5) -> 00235
//          func padLeft(235,3) -> 235
//          func padLeft(235,6) -> 000235

var lib = require('../lib/h.lib');

console.log(lib.padLeft(235, 5));
console.log(lib.padLeft(235, 3));
console.log(lib.padLeft(235, 6));
console.log(lib.padLeft(-235, 6));
