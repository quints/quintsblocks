// 7. An unsorted array has 1 to 1000 numbers, except one number. Find the missing number.

var lib = require('../lib/g.lib')

var unsortedArray1 = [10,6,3,8,4,7,9,2,1];
console.log(lib.findMissingNumber(unsortedArray1));
