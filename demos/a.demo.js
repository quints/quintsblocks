// 1.
// Complete the function caffeineBuzz, which takes a non-zero integer as it's one argument.
//   If the integer is divisible by 3, return the string "Java".
//   If the integer is divisible by 3 and divisible by 4, return the string"Coffee"
//   If the integer is one of the above and is even, add "Script" to the end of the string.
//   Otherwise, return the string "mocha_missing!"
//   caffeineBuzz(1)   => "mocha_missing!"
//   caffeineBuzz(3)   => "Java"
//   caffeineBuzz(6)   => "JavaScript"
//   caffeineBuzz(12)  => "CoffeeScript"


var lib = require('../lib/a.lib');

console.log(lib.caffeineBuzz(1));
console.log(lib.caffeineBuzz(3));
console.log(lib.caffeineBuzz(6));
console.log(lib.caffeineBuzz(12));
console.log(lib.caffeineBuzz("acv"));
