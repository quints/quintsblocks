// 3. Given 2 arrays find the values not present in the second one. eg [1,2,3,4,5] [4,5,6,7,8] = 1,2,3
// 4. Given 2 arrays find the values common in both. eg [1,2,3,4,5] [4,5,6,7,8] = 5

var lib = require('../lib/c.lib')

var arr1 = [1,2,3,4,5];
var arr2 = [4,5,6,7,8];

var result1 = arr1.diff(arr2, lib.diffModesEnum.uniqueInArray1)
console.log(result1.uniqueInArray1);

var result2 = arr1.diff(arr2, lib.diffModesEnum.uniqueInArray2)
console.log(result2.uniqueInArray2);

var result3 = arr1.diff(arr2, lib.diffModesEnum.intersection)
console.log(result3.intersection);

var result4 = arr1.diff(arr2, lib.diffModesEnum.uniqueInArray1 | lib.diffModesEnum.intersection)
console.log(result4.uniqueInArray1);
console.log(result4.intersection);

var result5 = arr1.diff(arr2)
console.log(result5.uniqueInArray1);
console.log(result5.uniqueInArray2);
console.log(result5.intersection);
