// 2. Identify and remove duplicates from an array/list.

var lib = require("../lib/b.lib");

console.log([1,2,3,4,5,6].unique());
console.log([1,2,3,4,5,6,5,6].unique());
console.log([1,2,3,4,5,6,1].unique());
console.log(["abc","def","ade","mno","ijk","wxy","pqr","ABC","ade","abc"].unique());
console.log([{a: 1, b: 2}, {a: 2, b: 3}, {a: 1, b: 2}, {c: 1, b: 2}, {A: 1, b: 2}].unique());
console.log([1,2,3,4,3,2,"mno","ijk","wxy","pqr","ABC","ade","abc", {a: 1, b: 2}, {a: 2, b: 3}, {a: 1, b: 2}, {c: 1, b: 2}, {A: 1, b: 2}, {b: 2, a: 1}].unique());


//TODO: try instead of taking second array, doing all with first array itself. maybe use delete operator
